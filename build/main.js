/**
 * Furniture Gallery
 */
document.querySelector('.second-item').classList.add('active');
document.querySelector('.second-slide').classList.add('active');

document.querySelectorAll('.carousel-nav-item').forEach(el => {
    el.addEventListener('click', selectFirstTabs);
});

function selectFirstTabs(event) {
    let target = event.target.dataset.target;

    if (target.tagName == 'SPAN') return;

    document.querySelectorAll('.carousel-nav-item, .this-slider').forEach(el => {
        el.classList.remove('active');
    });

    event.target.classList.add('active');
    document.querySelector('.' + target).classList.add('active');
}