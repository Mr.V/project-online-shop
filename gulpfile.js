const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

function copyHtml() {
    return gulp.src('src/index.html')
        .pipe(gulp.dest('./'))
}

function watch() {
    gulp.watch('src/index.html', copyHtml).on('change', browserSync.reload);
    gulp.watch('src/scss/*.scss', sassToCss)
}

function serve() {
    browserSync.init({
        server: './'
    });
    watch();
}

function sassToCss() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('build'))
        .pipe(browserSync.stream())
}

function copyJsLibs() {
    return gulp.src([
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/bootstrap/dist/js/bootstrap.min.js',
            './node_modules/popper.js/dist/umd/popper.min.js',
            './node_modules/util-js/src/Util.js',
            './src/js/*main.js'
        ])
        .pipe(gulp.dest('./build'))
}

function imgBuild() {
    return gulp.src('src/img/**/*.*')
        .pipe(gulp.dest('./build/img'))
}

exports.build = gulp.series(copyHtml, sassToCss, copyJsLibs, imgBuild);
exports.watch = watch;
exports.sass = sassToCss;
exports.serve = serve;